defmodule Http.Application do
  @moduledoc false

  use Application

  @sol_socket 1
  @so_reuseport 15

  @socket_num 36

  def start(_type, _args) do
    state = :ets.new(:state, [:set, :named_table, :public])
    :ets.insert(state, {:counter, 0})

    dispatch =
      :cowboy_router.compile([
        {:_,
         [
           {"/", Worker, state}
         ]}
      ])

    children =
      0..(@socket_num - 1)
      |> Enum.map(fn i ->
        id = :"http#{i}"

        %{
          id: id,
          start:
            {:cowboy, :start_clear,
             [
               id,
               %{
                 max_connections: 1_000_000,
                 socket_opts: [{:port, 8080}, {:raw, @sol_socket, @so_reuseport, <<1::size(32)>>}]
               },
               %{max_keepalive: 1_000, env: %{dispatch: dispatch}}
             ]},
          restart: :permanent,
          shutdown: :infinity,
          type: :supervisor
        }
      end)

    opts = [strategy: :one_for_one, name: Http.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
defmodule GlobalId do
  require Logger

  @moduledoc """
  GlobalId module contains an implementation of a guaranteed globally unique id system.
  """

  @doc """
  Please implement the following function.
  64 bit non negative integer output
  """
  @spec get_id(number) :: non_neg_integer
  def get_id(number) do
    {:ok, id} = node_id()
    <<result::64>> = <<id::size(10),timestamp()::size(34),number::size(20)>>
    result
  end

  #
  # You are given the following helper functions
  # Presume they are implemented - there is no need to implement them.
  #

  @doc """
  Returns your node id as an integer.
  It will be greater than or equal to 0 and less than or equal to 1024.
  It is guaranteed to be globally unique.
  """
  @spec node_id() :: non_neg_integer
  def node_id do
    contents = Enum.at(String.split(System.get_env("HOSTNAME"), "-"), 1)

    case Integer.parse(contents) do
      {result, ""} -> {:ok, result}
      {_, _rest} -> {:error, :unparsable}
      :error -> {:error, :unparsable}
    end
  end

  @doc """
  Returns timestamp since the epoch in milliseconds.
  """
  @spec timestamp() :: non_neg_integer
  def timestamp do
    :os.system_time(:seconds)
  end
end

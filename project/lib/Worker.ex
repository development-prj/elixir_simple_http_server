defmodule Worker do
  @moduledoc false
  require Logger

  def init(req, state) do
    [{:counter, counter}] = :ets.lookup(state, :counter)
    Logger.debug("Counter is #{counter}")

    req =
      :cowboy_req.reply(
        200,
        %{"content-type" => "text/plain"},
        "#{Integer.to_string(GlobalId.get_id(counter))}",
        req
      )
    new_state = :ets.insert(state, {:counter, counter+1})
    {:ok, req, new_state}
  end
end

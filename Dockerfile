FROM elixir:1.9 as build
MAINTAINER Stefan Stan <19stephan93@gmail.com>

ARG _dev_cookie=""
ARG _prod_cookie=""

ENV DEV_COOKIE=$_dev_cookie
ENV PROD_COOKIE=$_prod_cookie

WORKDIR /app
COPY project /app/

#Install dependencies and build Release
RUN export MIX_ENV=prod && \
    rm -Rf _build && \
    mix local.hex --force && \
    mix local.rebar --force && \
    mix deps.get && \
    mix release

#Make archive
RUN mkdir /export && \
    tar -cf /export/http.tar.gz _build/prod/rel/http

#================
#Deployment Stage
#================
FROM elixir:1.9

#Set environment variables and expose port
ENV REPLACE_OS_VARS=true
EXPOSE 8080

WORKDIR /app
COPY --from=build /export/ /app
RUN tar -xf http.tar.gz

#Set default entrypoint and command
ENTRYPOINT ["/app/_build/prod/rel/http/bin/http", "start"]
CMD ["foreground"]